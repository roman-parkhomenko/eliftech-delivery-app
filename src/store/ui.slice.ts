import { createSlice, PayloadAction } from '@reduxjs/toolkit';

export type NotificationTypes = {
  status: 'error' | 'success' | 'pending';
  title: 'Error!' | 'Success!' | 'Sending...';
  message: string;
};
type InitialUiState = {
  notification: NotificationTypes | null;
};
const initialState: InitialUiState = {
  notification: null
};
const uiSlice = createSlice({
  name: 'ui',
  initialState,
  reducers: {
    showNotification(state, { payload }: PayloadAction<NotificationTypes>) {
      state.notification = {
        status: payload.status,
        title: payload.title,
        message: payload.message
      };
    },
    clearNotification(state) {
      state.notification = null;
    }
  }
});
export const uiActions = uiSlice.actions;

export default uiSlice.reducer;
