import { Dispatch } from '@reduxjs/toolkit';

import { uiActions } from './ui.slice';
import { CartItemTypes } from './cart.slice';

export type UserForm = {
  name: string;
  email: string;
  phone: string;
  address: string;
};
type Order = {
  userInfo: UserForm;
  cartItems: CartItemTypes[];
};
export const sendOrdersData = (order: Order) => {
  return async (dispatch: Dispatch) => {
    dispatch(
      uiActions.showNotification({
        status: 'pending',
        title: 'Sending...',
        message: 'We are processing your order. Please wait while we send the order data.'
      })
    );
    const sendRequest = async () => {
      const response = await fetch(
        'https://eliftech-delivery-app-72f3f-default-rtdb.europe-west1.firebasedatabase.app/orders.json',
        {
          method: 'POST',
          body: JSON.stringify(order)
        }
      );
      if (!response.ok) {
        throw new Error('Sending order data failed');
      }
    };
    try {
      await sendRequest();
      dispatch(
        uiActions.showNotification({
          status: 'success',
          title: 'Success!',
          message:
            'Thank you for your order! Your order data has been successfully sent. Please wait for our imaginary employee to contact you.'
        })
      );
      setTimeout(() => dispatch(uiActions.clearNotification()), 10000);
    } catch (error) {
      dispatch(
        uiActions.showNotification({
          status: 'error',
          title: 'Error!',
          message: 'Sending order data failed!'
        })
      );
    }
  };
};
