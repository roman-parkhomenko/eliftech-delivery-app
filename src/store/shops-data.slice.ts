import { createSlice } from '@reduxjs/toolkit';

export type ProductTypes = {
  id: string;
  title: string;
  price: number;
  imgUrl: string;
  quantityLeft: number;
};
export type ShopTypes = {
  id: string;
  name: string;
  products: ProductTypes[];
};
const initialState: Record<string, ShopTypes> = {};
const shopsDataSlice = createSlice({ name: 'shops-data', initialState, reducers: {} });

export default shopsDataSlice.reducer;
