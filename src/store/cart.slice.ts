import { createSlice, PayloadAction } from '@reduxjs/toolkit';

export type CartItemTypes = {
  name: string;
  price: number;
  imgUrl: string;
  quantity: number;
  brand: string;
};

export type CartStateTypes = {
  items: CartItemTypes[];
  currentShop: string;
  changed: boolean;
  totalQuantity: number;
  totalAmount: number;
};
const initialState: CartStateTypes = {
  items: [],
  currentShop: '',
  changed: false,
  totalAmount: 0,
  totalQuantity: 0
};

const cartSlice = createSlice({
  name: 'cart',
  initialState,
  reducers: {
    add(state, { payload }: PayloadAction<CartItemTypes>) {
      const { name, price, brand, imgUrl } = payload;
      const existingItem = state.items.find((item) => item.name === name);
      state.changed = true;
      if (state.items.length === 0) {
        state.currentShop = brand;
      }
      if (!existingItem && (state.currentShop === '' || state.currentShop === brand)) {
        state.items.push({
          name,
          price,
          imgUrl,
          brand,
          quantity: 1
        });
      } else if (existingItem && state.currentShop === brand) {
        existingItem.quantity += 1;
      }
    },
    remove(state, action: PayloadAction<string>) {
      const title = action.payload;
      const existingItem = state.items.find((item) => item.name === title);
      state.changed = true;

      if (state.items.length === 1 && existingItem && existingItem.quantity === 1) {
        state.currentShop = '';
      }

      if (existingItem && existingItem.quantity === 1) {
        state.items = state.items.filter((item) => item.name !== title);
      } else if (existingItem && existingItem.quantity > 1) {
        existingItem.quantity -= 1;
      }
    },
    clear(state) {
      state.items = [];
      state.currentShop = '';
      state.changed = false;
      state.totalAmount = 0;
      state.totalQuantity = 0;
    },
    countTotals(state) {
      state.totalQuantity = state.items.reduce((acc, item) => acc + item.quantity, 0);
      state.totalAmount = Number(
        state.items.reduce((acc, item) => acc + item.quantity * item.price, 0).toFixed(2)
      );
    }
  }
});

export const cartActions = cartSlice.actions;

export default cartSlice.reducer;
