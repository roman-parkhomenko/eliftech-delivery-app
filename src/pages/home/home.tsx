import { useEffect } from 'react';
import { Outlet, useLocation } from 'react-router-dom';

import useHttp from '../../hooks/use-http';
import { getShopsList } from '../../utils/api';
import { useAppSelector } from '../../store/store';

import LoadingSpinner from '../../components/ui/loading-spinner';

import { ShopLink, ShopListItem, ShopWrapper } from './home.styles';
import { CardWrapper } from '../../global.styles';

export type LoadedShopsListTypes = { pathToShop: string; shopName: string };
const Home = () => {
  const location = useLocation();
  const {
    sendRequest,
    status,
    data: loadedShopsList,
    error
  } = useHttp(getShopsList, true);
  useEffect(() => {
    sendRequest();
  }, [sendRequest]);

  const currentShop = useAppSelector(({ cart }) => cart.currentShop);

  if (status === 'pending') {
    return (
      <div>
        <LoadingSpinner />
      </div>
    );
  }
  if (error) {
    return <p>{error}</p>;
  }

  return (
    <ShopWrapper>
      <CardWrapper>
        <ul>
          <h2>Shops:</h2>
          {loadedShopsList.map(({ pathToShop, shopName }: LoadedShopsListTypes) => (
            <ShopLink
              to={pathToShop}
              onClick={(event) => {
                if (currentShop && currentShop !== pathToShop) {
                  event.preventDefault();
                }
              }}
              key={pathToShop}
            >
              <ShopListItem
                isSelecting={Boolean(!currentShop)}
                isChosen={Boolean(currentShop && currentShop === pathToShop)}
              >
                {shopName}
              </ShopListItem>
            </ShopLink>
          ))}
        </ul>
      </CardWrapper>
      {location.pathname === '/' && (
        <CardWrapper>
          <h2>Choose your today's meals 😋</h2>
        </CardWrapper>
      )}
      {location.pathname !== '/' && <Outlet />}
    </ShopWrapper>
  );
};
export default Home;
