import styled from 'styled-components';
import { Link } from 'react-router-dom';

export const ShopWrapper = styled.div`
  display: grid;
  height: 100%;
  gap: 5px;
  padding: 5px;
  grid-template-rows: auto 1fr;

  @media screen and (min-width: 768px) {
    grid-template-columns: auto 1fr;
    grid-template-rows: 1fr;
    padding: 10px;
    gap: 10px;
  }
`;
export const ShopLink = styled(Link)`
  font-size: 1.2rem;
  font-weight: bold;
  cursor: pointer;
  -webkit-user-select: none; /* Safari */
  -ms-user-select: none; /* IE 10 and IE 11 */
  user-select: none; /* Standard syntax */

  @media screen and (min-width: 768px) {
    font-size: 1.5rem;
    padding: 0 15px;
  }
`;

type ShopListItemProps = {
  isSelecting: boolean;
  isChosen: boolean;
};
export const ShopListItem = styled.div<ShopListItemProps>`
  border: 3px black solid;
  padding: 0 5px;

  border-radius: 10px;
  margin-bottom: 10px;
  background-color: #bbb;
  transition: all ease-in-out 0.3s;
  color: ${({ isChosen, isSelecting }) => {
    if (isSelecting) {
      return '#444';
    }
    if (isChosen) {
      return '#1397d7';
    } else {
      return '#ccc';
    }
  }};

  &:hover {
    background-color: ${({ isSelecting }) => (isSelecting ? '#eee' : '#bbb')};
  }
  &:hover {
    cursor: ${({ isSelecting, isChosen }) => {
      if (isSelecting) {
        return 'pointer';
      }
      if (isChosen) {
        return 'pointer';
      } else {
        return 'not-allowed';
      }
    }};
  }
`;
