import { ChangeEvent, FormEvent, useEffect, useState } from 'react';

import { getUserOrders } from '../../utils/api';
import { CartItemTypes } from '../../store/cart.slice';
import { UserForm } from '../../store/cart.actions';

import FormInput from '../../components/form-input/form-input';
import LoadingSpinner from '../../components/ui/loading-spinner';
import Button from '../../components/ui/button';
import HistoryOrder from '../../components/history/history-order';

import { HistoryWrapper } from './history.styles';
import { CardWrapper } from '../../global.styles';

const defaultFormFields = {
  email: '',
  phone: ''
};
export type FetchedOrder = {
  id: string;
  userInfo: UserForm;
  cartItems: CartItemTypes[];
  date: Date;
  address: string;
};
let isInitial = true;
const History = () => {
  const [formFields, setFormFields] = useState(defaultFormFields);
  const [isPending, setIsPending] = useState(false);
  const [userOrders, setUserOrders] = useState<FetchedOrder[]>([]);
  const { email, phone } = formFields;

  useEffect(() => {
    return () => {
      isInitial = true;
    };
  }, []);

  const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
    const { name, value } = event.target;

    setFormFields({ ...formFields, [name]: value });
  };
  const submitHandler = async (event: FormEvent) => {
    setIsPending(true);
    event.preventDefault();
    const loadedUserOrders = await getUserOrders({ email, phone });
    setUserOrders(loadedUserOrders as FetchedOrder[] | any);
    setIsPending(false);
    isInitial = false;
  };
  return (
    <HistoryWrapper>
      <CardWrapper>
        <form onSubmit={submitHandler}>
          <FormInput
            label="Email:"
            type="email"
            name="email"
            onChange={handleChange}
            value={email}
            autoComplete="email"
            required
          />
          <FormInput
            label="Phone:"
            type="tel"
            name="phone"
            onChange={handleChange}
            value={phone}
            autoComplete="tel"
            required
          />
          <Button>Search orders</Button>
        </form>
      </CardWrapper>
      <CardWrapper>
        {isPending && <LoadingSpinner />}
        {userOrders.length === 0 && isInitial && !isPending && (
          <h2>
            To search your previous orders, please fill out the form above and click
            submit.
          </h2>
        )}
        {userOrders?.length === 0 && !isInitial && !isPending && (
          <h2>
            We couldn't find any orders associated with the provided Email and Phone
            number. Please double-check your information and try again.
          </h2>
        )}
        {userOrders.length > 0 && !isPending && (
          <h2>Found orders matching your search criteria:</h2>
        )}
        {userOrders.length > 0 &&
          !isPending &&
          userOrders.map((order) => <HistoryOrder userOrder={order} key={order.id} />)}
      </CardWrapper>
    </HistoryWrapper>
  );
};

export default History;
