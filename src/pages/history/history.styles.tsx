import styled from 'styled-components';

export const HistoryWrapper = styled.section`
  display: grid;
  height: 100%;
  gap: 5px;
  padding: 5px;
  grid-template-rows: auto 1fr;

  @media screen and (min-width: 768px) {
    grid-template-columns: 1fr 2fr;
    grid-template-rows: 1fr;
    padding: 10px;
    gap: 10px;
  }
`;
