import { useAppDispatch, useAppSelector } from '../../store/store';
import { cartActions } from '../../store/cart.slice';

import UserInfo from '../../components/cart/user-info';
import CartContainer from '../../components/cart/cart-container';
import Button, { BUTTON_TYPE_CLASSES } from '../../components/ui/button';

import { ButtonsContainer, CartWrapper, TotalPrice } from './cart.styles';

const Cart = () => {
  const dispatch = useAppDispatch();
  const totalAmount = useAppSelector(({ cart }) => cart.totalAmount);
  const clearCart = async () => {
    await dispatch(cartActions.clear());
  };

  return (
    <>
      <CartWrapper>
        <UserInfo />
        <CartContainer />
        <TotalPrice>Total price: ${totalAmount.toFixed(2)}</TotalPrice>
        {Boolean(totalAmount) && (
          <ButtonsContainer>
            <Button onClick={clearCart} buttonType={BUTTON_TYPE_CLASSES.inverted}>
              Clear Cart
            </Button>
            <Button form="submit-form">Submit</Button>
          </ButtonsContainer>
        )}
      </CartWrapper>
    </>
  );
};
export default Cart;
