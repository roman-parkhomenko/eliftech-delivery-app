import styled from 'styled-components';

export const CartWrapper = styled.div`
  display: grid;
  height: 100%;
  gap: 5px;
  padding: 5px;
  grid-template-rows: auto auto 1fr;

  @media screen and (min-width: 768px) {
    grid-template-columns: 1fr 2fr;
    grid-template-rows: 1fr auto;
    padding: 10px;
    gap: 10px;
  }
`;
export const TotalPrice = styled.span`
  font-size: 2rem;
  font-weight: bold;
  cursor: default;
`;
export const ButtonsContainer = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  gap: 5px;
`;
