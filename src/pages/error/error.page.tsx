import { CardWrapper, GlobalStyles } from '../../global.styles';
import Header from '../../components/header/header';
import { ErrorContent, ErrorTitle } from './error.styles';
import { useRouteError, isRouteErrorResponse } from 'react-router-dom';

const ErrorPage = () => {
  const error = useRouteError();
  let title: string;
  let message: string;

  if (isRouteErrorResponse(error) && error.status === 404) {
    title = error.statusText;
    message = error.error ? error.error.message : 'Could not find resource or page.';
  } else if (isRouteErrorResponse(error)) {
    title = error.statusText;
    message = error.error ? error.error.message : 'Something went wrong!';
  } else {
    title = 'An error occurred!';
    message = 'Something went wrong!';
  }

  return (
    <>
      <GlobalStyles />
      <Header />
      <ErrorContent>
        <CardWrapper>
          <ErrorTitle>{title}</ErrorTitle>
          <h3>{message}</h3>
        </CardWrapper>
      </ErrorContent>
    </>
  );
};
export default ErrorPage;
