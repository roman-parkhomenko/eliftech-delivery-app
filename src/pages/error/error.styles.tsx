import styled from 'styled-components';

export const ErrorContent = styled.div`
  padding: 15px;
  text-align: center;
`;
export const ErrorTitle = styled.h1`
  margin-top: 25vh;
`;
