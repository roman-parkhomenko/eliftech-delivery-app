import { LoadedShopsListTypes } from '../pages/home/home';

const FIREBASE_DOMAIN =
  'https://eliftech-delivery-app-72f3f-default-rtdb.europe-west1.firebasedatabase.app/';

export async function getShopsList() {
  const response = await fetch(`${FIREBASE_DOMAIN}/shops-data.json`);
  const data = await response.json();
  if (!response.ok) {
    throw new Error(data.message || 'Could not fetch shops data.');
  }
  const shopList = Object.keys(data).map(
    (shop: keyof typeof data) =>
      ({
        pathToShop: shop,
        shopName: data[shop].name
      } as LoadedShopsListTypes)
  );

  return shopList;
}

export async function getSingleShopData(shopId: string) {
  const response = await fetch(`${FIREBASE_DOMAIN}/shops-data/${shopId}.json`);
  const data = await response.json();

  if (!response.ok) {
    throw new Error(data.message || `Could not fetch ${shopId}'s data.`);
  }

  const loadedShopData = {
    id: shopId,
    ...data
  };
  return loadedShopData;
}

export async function getUserOrders(user: { email: string; phone: string }) {
  const response = await fetch(`${FIREBASE_DOMAIN}/orders.json`);

  const data = await response.json();

  if (!response.ok) {
    throw new Error(data.message || 'Could not find orders .');
  }

  const allOrders = [];
  for (const key in data) {
    const orderObj = {
      id: key,
      ...data[key]
    };

    allOrders.push(orderObj);
  }
  const userOrders = allOrders.filter(
    (order) => order.phone === user.phone && order.email === user.email
  );

  return userOrders;
}
