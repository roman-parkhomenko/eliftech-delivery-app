export const getDate = (propDate: Date) => {
  const dateString = new Date(propDate);

  if (!dateString?.valueOf()) return 'date is unknown ☹';

  return dateString.toLocaleDateString('us', {
    year: 'numeric',
    month: 'long',
    day: 'numeric',
    hour: '2-digit',
    minute: '2-digit',
    hour12: false
  });
};
