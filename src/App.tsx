import React from 'react';
import { createBrowserRouter, RouterProvider } from 'react-router-dom';

import Layout from './components/layout/layout';
import Home from './pages/home/home';
import Cart from './pages/cart/cart';
import History from './pages/history/history';
import Shop from './components/shop/shop';
import ErrorPage from './pages/error/error.page';

const router = createBrowserRouter([
  {
    path: '/',
    Component: Layout,
    errorElement: <ErrorPage />,
    children: [
      {
        path: '',
        Component: Home,
        children: [{ path: ':id', Component: Shop }]
      },
      { path: 'cart', Component: Cart },
      { path: 'history', Component: History }
    ]
  }
]);

function App() {
  return <RouterProvider router={router} />;
}

export default App;
