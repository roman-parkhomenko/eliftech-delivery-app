import { Spinner } from './loading-spinner.styles';

const LoadingSpinner = () => {
  return <Spinner />;
};

export default LoadingSpinner;
