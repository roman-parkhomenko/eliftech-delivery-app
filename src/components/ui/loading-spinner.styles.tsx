import styled, { keyframes } from 'styled-components';

const spin = keyframes`
  0% {
    transform: rotate(0deg);
  }
  100% {
    transform: rotate(360deg);
  }`;
export const Spinner = styled.div`
  width: 80px;
  height: 80px;
  margin: 3rem auto;
  text-align: center;
  display: flex;
  justify-content: center;
  align-items: center;
  &:after {
    content: ' ';
    display: block;
    width: 64px;
    height: 64px;
    margin: 8px;
    border-radius: 50%;
    border: 6px solid #1b699a;
    border-color: #1b699a transparent #1b699a transparent;
    animation: ${spin} 1.2s linear infinite;
  }
`;
