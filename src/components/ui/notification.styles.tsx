import styled from 'styled-components';

type NotificationProps = { status: 'error' | 'success' | 'pending' };
export const NotificationContainer = styled.section<NotificationProps>`
  width: 100%;
  height: auto;
  padding: 2rem 10%;
  background-color: ${({ status }) => {
    if (status === 'pending') {
      return '#444';
    }
    if (status === 'success') {
      return '#1397d7';
    }
    if (status === 'error') {
      return '#690000';
    }
  }};
  border-radius: 1rem;
  box-shadow: #fff 0 0 50px -15px;

  display: flex;
  justify-content: space-around;
  flex-wrap: wrap;
  align-items: center;

  & > h2,
  p {
    color: #eee;
  }
`;
