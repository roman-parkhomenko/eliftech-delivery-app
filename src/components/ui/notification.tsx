import { FC } from 'react';
import { NotificationContainer } from './notification.styles';
import { NotificationTypes } from '../../store/ui.slice';

const Notification: FC<NotificationTypes> = ({ status, title, message }) => {
  return (
    <NotificationContainer status={status}>
      <h2>{title}</h2>
      <p>{message}</p>
    </NotificationContainer>
  );
};

export default Notification;
