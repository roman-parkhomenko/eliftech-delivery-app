import { createPortal } from 'react-dom';

import { BackdropElement, ModalElement } from './modal.styles';
import { FC, ReactNode } from 'react';

type BackdropProps = {
  closeModal?: () => void;
};
const Backdrop: FC<BackdropProps> = ({ closeModal }) => {
  return <BackdropElement onClick={closeModal} />;
};

type ModalOverlayProps = {
  children: ReactNode;
};
const ModalOverlay: FC<ModalOverlayProps> = ({ children }) => {
  return <ModalElement>{children}</ModalElement>;
};

const portalElement = document.getElementById('overlays') as HTMLElement;

type ModalProps = {
  closeModal?: () => void;
  children: ReactNode;
};
const Modal: FC<ModalProps> = ({ closeModal, children }) => {
  return (
    <>
      {createPortal(<Backdrop closeModal={closeModal} />, portalElement)}
      {createPortal(<ModalOverlay>{children}</ModalOverlay>, portalElement)}
    </>
  );
};

export default Modal;
