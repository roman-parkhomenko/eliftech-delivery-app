import { useEffect } from 'react';
import { useParams } from 'react-router-dom';

import useHttp from '../../hooks/use-http';
import { getSingleShopData } from '../../utils/api';
import { ProductTypes } from '../../store/shops-data.slice';

import LoadingSpinner from '../ui/loading-spinner';
import ProductItem from './product-item';

import { CardWrapper } from '../../global.styles';
import { ProductsWrapper } from './shop.styles';

const Shop = () => {
  const { id } = useParams();
  const {
    sendRequest,
    status,
    data: loadedShopData,
    error
  } = useHttp(getSingleShopData, true);

  useEffect(() => {
    sendRequest(id);
  }, [sendRequest, id]);

  if (status === 'pending') {
    return (
      <div>
        <LoadingSpinner />
      </div>
    );
  }
  if (error) {
    return <p>{error}</p>;
  }

  if (!loadedShopData.name) {
    return (
      <CardWrapper>
        <h2>
          Sorry! We can't deliver goods from "
          {loadedShopData.id
            .split('_')
            .map((word: string) => word[0].toLocaleUpperCase() + word.slice(1))
            .join(' ')}
          " shop
        </h2>
      </CardWrapper>
    );
  }

  return (
    <CardWrapper>
      <ProductsWrapper>
        {loadedShopData.products.map((product: ProductTypes) => (
          <ProductItem product={product} key={product.title} brand={id as string} />
        ))}
      </ProductsWrapper>
    </CardWrapper>
  );
};
export default Shop;
