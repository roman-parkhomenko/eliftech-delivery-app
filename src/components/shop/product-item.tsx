import { FC } from 'react';

import { useAppDispatch } from '../../store/store';
import { cartActions } from '../../store/cart.slice';
import { ProductTypes } from '../../store/shops-data.slice';

import Button, { BUTTON_TYPE_CLASSES } from '../ui/button';

import {
  Footer,
  Name,
  Price,
  ProductCardContainer,
  ProductImage
} from './product-item.styles';

type ProductItemProps = {
  product: ProductTypes;
  brand: string;
};
const ProductItem: FC<ProductItemProps> = ({ product, brand }) => {
  const dispatch = useAppDispatch();
  const { title, price, imgUrl } = product;
  const addToCartHandler = () => {
    dispatch(cartActions.add({ name: title, imgUrl, price, brand, quantity: 1 }));
    dispatch(cartActions.countTotals());
  };
  return (
    <ProductCardContainer>
      <ProductImage src={imgUrl} alt={title} />
      <Footer>
        <Name>{title}</Name>
        <Price>${price}</Price>
      </Footer>
      <Button buttonType={BUTTON_TYPE_CLASSES.inverted} onClick={addToCartHandler}>
        add to Cart
      </Button>
    </ProductCardContainer>
  );
};
export default ProductItem;
