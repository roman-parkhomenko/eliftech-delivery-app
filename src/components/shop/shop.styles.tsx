import styled from 'styled-components';

export const ProductsWrapper = styled.div`
  @media screen and (min-width: 768px) {
    display: grid;
    grid-template-columns: repeat(2, 1fr);
    grid-template-rows: auto auto 1fr;
    gap: 10px;
  }
  @media screen and (min-width: 1230px) {
    grid-template-columns: repeat(3, 1fr);
  }
`;
