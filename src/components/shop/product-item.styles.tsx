import styled from 'styled-components';

export const ProductCardContainer = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  height: 350px;
  align-items: center;
  position: relative;
  margin-bottom: 20px;

  button {
    width: 80%;
    opacity: 0.7;
    position: absolute;
    top: 255px;
    display: none;
  }
  &:hover {
    img {
      opacity: 0.8;
    }
    button {
      opacity: 0.85;
      display: flex;
    }
  }
  @media screen and (max-width: 768px) {
    button {
      display: block;
      opacity: 0.9;
      min-width: unset;
      padding: 0;
      width: 90%;

      &:hover {
        img {
          opacity: unset;
        }
        button {
          opacity: unset;
        }
      }
    }
  }
  @media screen and (max-width: 300px) {
    width: 80vw;
  }
`;
export const ProductImage = styled.img`
  width: 100%;
  height: 95%;
  object-fit: cover;
  margin-bottom: 5px;
`;
export const Footer = styled.div`
  width: 100%;
  height: 5%;
  display: flex;
  justify-content: space-between;
  font-size: 1.2rem;
  cursor: default;

  @media screen and (min-width: 1230px) {
    font-size: 1.5rem;
  }
`;
export const Name = styled.span`
  width: 90%;
  margin-bottom: 25px;
`;
export const Price = styled.span`
  width: auto;
`;
