import styled from 'styled-components';

export const CheckoutItemContainer = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-evenly;
  min-height: 100px;
  border-bottom: 1px solid darkgrey;
  padding: 15px 0;
  font-size: 1.3rem;
  align-items: center;
`;
export const ImageContainer = styled.div`
  width: 25%;

  img {
    width: 100%;
    height: 100%;
  }
`;

export const Name = styled.span`
  width: 25%;
  text-align: center;
  cursor: default;
`;
export const Price = styled(Name)``;
export const QuantityWrapper = styled(Name)`
  display: flex;
  justify-content: center;
`;
export const Arrow = styled.div`
  cursor: pointer;
  -webkit-user-select: none; /* Safari */
  -ms-user-select: none; /* IE 10 and IE 11 */
  user-select: none; /* Standard syntax */
`;
export const Value = styled.span`
  margin: 0 10px;
`;
