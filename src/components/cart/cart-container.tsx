import { useAppSelector } from '../../store/store';

import CartItem from './cart-item';

import { CardWrapper } from '../../global.styles';

const CartContainer = () => {
  const { items: products, totalQuantity } = useAppSelector(({ cart }) => cart);
  const message = `Your cart${Boolean(totalQuantity) ? ':' : ' is empty!'}`;

  return (
    <CardWrapper>
      <h2>{message}</h2>
      {products.map((product, index) => (
        <CartItem key={index} product={product} />
      ))}
    </CardWrapper>
  );
};
export default CartContainer;
