import { FC } from 'react';

import { useAppDispatch } from '../../store/store';

import { cartActions, CartItemTypes } from '../../store/cart.slice';

import {
  Arrow,
  CheckoutItemContainer,
  ImageContainer,
  Name,
  Price,
  QuantityWrapper,
  Value
} from './cart-item.styles';

type CartItemProps = {
  product: CartItemTypes;
};
const CartItem: FC<CartItemProps> = ({
  product: { name, price, imgUrl, quantity, brand }
}) => {
  const dispatch = useAppDispatch();
  const { add, remove, countTotals } = cartActions;
  const addItemToCartHandler = () => {
    dispatch(add({ name, price, imgUrl, brand, quantity: 1 }));
    dispatch(countTotals());
  };
  const removeItemFromCartHandler = () => {
    dispatch(remove(name));
    dispatch(countTotals());
  };
  return (
    <CheckoutItemContainer>
      <ImageContainer>
        <img src={imgUrl} alt={name} />
      </ImageContainer>
      <Name>{name}</Name>
      <Price>${price}</Price>
      <QuantityWrapper>
        <Arrow onClick={removeItemFromCartHandler}>&#10094;</Arrow>
        <Value>{quantity}</Value>
        <Arrow onClick={addItemToCartHandler}>&#10095;</Arrow>
      </QuantityWrapper>
    </CheckoutItemContainer>
  );
};
export default CartItem;
