import { ChangeEvent, FormEvent, useState } from 'react';

import { useAppDispatch, useAppSelector } from '../../store/store';
import { sendOrdersData } from '../../store/cart.actions';

import FormInput from '../form-input/form-input';

import { cartActions } from '../../store/cart.slice';

import { CardWrapper } from '../../global.styles';
import Modal from '../ui/modal';
import Notification from '../ui/notification';
import { uiActions } from '../../store/ui.slice';

export const defaultFormFields = {
  name: '',
  email: '',
  phone: '',
  address: ''
};
const UserInfo = () => {
  const notification = useAppSelector(({ ui }) => ui.notification);

  const cartItems = useAppSelector(({ cart }) => cart.items);
  const [formFields, setFormFields] = useState(defaultFormFields);
  const { name, email, phone, address } = formFields;
  const dispatch = useAppDispatch();
  const resetFormFields = () => {
    setFormFields(defaultFormFields);
  };

  const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
    const { name, value } = event.target;

    setFormFields({ ...formFields, [name]: value });
  };
  const submitHandler = (event: FormEvent) => {
    event.preventDefault();
    const order = { ...formFields, cartItems, date: new Date() };
    // @ts-ignore
    dispatch(sendOrdersData(order));
    dispatch(cartActions.clear());
    resetFormFields();
  };
  const closeModalHandler = () => {
    dispatch(uiActions.clearNotification());
  };
  return (
    <CardWrapper>
      <form id="submit-form" onSubmit={submitHandler}>
        <FormInput
          label="Name:"
          type="text"
          name="name"
          onChange={handleChange}
          value={name}
          autoComplete="username"
          required
        />
        <FormInput
          label="Email:"
          type="email"
          name="email"
          onChange={handleChange}
          value={email}
          autoComplete="email"
          required
        />
        <FormInput
          label="Phone:"
          type="tel"
          name="phone"
          onChange={handleChange}
          value={phone}
          autoComplete="tel"
          required
        />
        <FormInput
          label="Address:"
          type="text"
          name="address"
          onChange={handleChange}
          value={address}
          autoComplete="address"
          required
        />
      </form>
      {notification && (
        <Modal closeModal={closeModalHandler}>
          <Notification
            status={notification.status}
            title={notification.title}
            message={notification.message}
          />
        </Modal>
      )}
    </CardWrapper>
  );
};
export default UserInfo;
