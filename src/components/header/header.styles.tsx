import styled from 'styled-components';
import { Link, NavLink } from 'react-router-dom';

export const HeaderContainer = styled.header`
  background: linear-gradient(180deg, #aaa, #fff0);
  grid-row: 1;
  grid-column: 1 / -1;
  display: flex;
  justify-content: space-between;
`;

export const LogoContainer = styled(Link)`
  height: 100%;
  padding: 10px 25px;
  svg {
    height: 100%;
  }
`;
export const NavBar = styled.nav`
  width: 50%;
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: flex-end;

  @media screen and (max-width: 800px) {
    width: 80%;
  }
`;

export const NavigationLink = styled(NavLink)`
  font-size: 1.5rem;
  font-weight: bold;
  padding: 10px 15px;
  cursor: pointer;

  &:hover {
    color: #222;
    @media screen and (min-width: 768px) {
      text-shadow: 0 0 5px #fff;
    }
  }

  &.active {
    color: #1b699a;
    text-shadow: 0 0 5px #fff;
  }
`;

export const CartIconContainer = styled(NavLink)`
  width: 45px;
  height: 45px;
  position: relative;
  display: flex;
  align-items: center;
  justify-content: center;
  cursor: pointer;
  svg {
    width: 40px;
    height: 40px;
  }
  &.active span {
    color: #1b699a;
    text-shadow: 0 0 3px #fff;
  }
`;
export const ItemCount = styled.span`
  position: absolute;
  font-size: 16px;
  font-weight: bold;
  bottom: 5px;
`;
