import React from 'react';

import { useAppSelector } from '../../store/store';

import { ReactComponent as LogoSvg } from '../../assets/logo-color.svg';
import { ReactComponent as ShoppingIcon } from '../../assets/shopping-bag.svg';

import {
  CartIconContainer,
  HeaderContainer,
  ItemCount,
  LogoContainer,
  NavBar,
  NavigationLink
} from './header.styles';

const Header = () => {
  const { totalQuantity, currentShop } = useAppSelector(({ cart }) => cart);
  return (
    <HeaderContainer>
      <LogoContainer to="/">
        <LogoSvg />
      </LogoContainer>
      <NavBar>
        <NavigationLink to={`/${currentShop}`}>Shop</NavigationLink>
        <CartIconContainer to="/cart">
          <ShoppingIcon />
          <ItemCount>{totalQuantity}</ItemCount>
        </CartIconContainer>
        <NavigationLink to="/history">History</NavigationLink>
      </NavBar>
    </HeaderContainer>
  );
};
export default Header;
