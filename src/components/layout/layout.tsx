import { Outlet } from 'react-router-dom';

import Header from '../header/header';

import { GlobalStyles } from '../../global.styles';
import { Main } from './layout.styles';

const Layout = () => {
  return (
    <>
      <GlobalStyles />
      <Header />
      <Main>
        <Outlet />
      </Main>
    </>
  );
};
export default Layout;
