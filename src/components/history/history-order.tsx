import { FC, Fragment } from 'react';

import { getDate } from '../../utils/get-date';

import HistoryItem from './history-item';

import { FetchedOrder } from '../../pages/history/history';

import { Horizon, Order } from './history-order.styles';

type HistoryOrderProps = {
  userOrder: FetchedOrder;
};
const HistoryOrder: FC<HistoryOrderProps> = ({ userOrder }) => {
  const { id, date, cartItems, address } = userOrder;
  return (
    <Order>
      <p>
        <b>Order ID:</b> {id}
      </p>
      <p>
        <b>Date:</b> {getDate(date)}
      </p>
      <p>
        <b>Address:</b> {address}
      </p>
      <p>
        <b>Total:</b> $
        {cartItems.reduce((acc, item) => acc + item.quantity * item.price, 0).toFixed(2)}
      </p>
      <p>
        <b>Items:</b>
      </p>
      {cartItems.map((item) => (
        <Fragment key={item.name}>
          <HistoryItem item={item} />
          <Horizon />
        </Fragment>
      ))}
    </Order>
  );
};

export default HistoryOrder;
