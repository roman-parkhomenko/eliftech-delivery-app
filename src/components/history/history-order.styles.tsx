import styled from 'styled-components';

export const Order = styled.div`
  margin-top: 1rem;
  padding: 10px;
  border-radius: 5px;
  box-shadow: 0 0 20px -8px black, inset white 0px 0px 20px 5px;
`;

export const Horizon = styled.hr`
  margin-top: 0;
`;
