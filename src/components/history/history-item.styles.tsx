import styled from 'styled-components';
export const Name = styled.span`
  font-size: 1.2rem;
  font-weight: bold;
`;
export const PriceAndQuantity = styled.span`
  font-style: italic;
`;
export const HistoryProduct = styled.div`
  padding: 0 15px;
  display: flex;
  justify-content: space-between;
`;
