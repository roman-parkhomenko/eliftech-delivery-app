import { FC } from 'react';

import { CartItemTypes } from '../../store/cart.slice';

import { HistoryProduct, Name, PriceAndQuantity } from './history-item.styles';

type HistoryItemProps = {
  item: CartItemTypes;
};
const HistoryItem: FC<HistoryItemProps> = ({ item }) => {
  return (
    <HistoryProduct key={item.name}>
      <Name>{item.name}</Name>
      <PriceAndQuantity>x{item.quantity}</PriceAndQuantity>
      <PriceAndQuantity>${item.price}</PriceAndQuantity>
    </HistoryProduct>
  );
};
export default HistoryItem;
