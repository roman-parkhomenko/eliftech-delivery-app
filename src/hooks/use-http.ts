import { useReducer, useCallback, Reducer } from 'react';

type HttpReducerState = {
  data: null | any;
  error: null | string;
  status: 'pending' | 'completed';
};
type HttpAction =
  | { type: 'SEND' }
  | { type: 'SUCCESS'; responseData: any }
  | { type: 'ERROR'; errorMessage: string };
const httpReducer: Reducer<HttpReducerState, HttpAction> = (state, action) => {
  if (action.type === 'SEND') {
    return {
      data: null,
      error: null,
      status: 'pending'
    };
  }

  if (action.type === 'SUCCESS') {
    return {
      data: action.responseData,
      error: null,
      status: 'completed'
    };
  }

  if (action.type === 'ERROR') {
    return {
      data: null,
      error: action.errorMessage,
      status: 'completed'
    };
  }

  return state;
};
type UseHttpRequestData = (requestData: any) => Promise<any>;
function useHttp(requestFunction: UseHttpRequestData, startWithPending = false) {
  const [httpState, dispatch] = useReducer(httpReducer, {
    status: startWithPending ? 'pending' : 'completed',
    data: null,
    error: null
  });

  const sendRequest = useCallback(
    async function (requestData: any | null = null) {
      dispatch({ type: 'SEND' });
      try {
        const responseData = await requestFunction(requestData);
        dispatch({ type: 'SUCCESS', responseData });
      } catch (error: any) {
        dispatch({
          type: 'ERROR',
          errorMessage: error.message || 'Something went wrong!'
        });
      }
    },
    [requestFunction]
  );

  return {
    sendRequest,
    ...httpState
  };
}

export default useHttp;
