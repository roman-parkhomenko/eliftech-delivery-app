import styled, { createGlobalStyle } from 'styled-components';

export const GlobalStyles = createGlobalStyle`
  /* Set default font and box-sizing */
  *,
  *::before,
  *::after {
    box-sizing: border-box;
    font-family: 'Roboto', sans-serif;
    color: #444;
  }

  /* Remove default margin and padding */
  html,
  body,
  #root {
    margin: 0;
    padding: 0;
  }

  /* Set background color */
  body {
    background-color: #eee;
    font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Oxygen',
    'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue',
    sans-serif;
  }

  #root {
    display: grid;
    grid-template-rows: auto 1fr;
    height: 100vh;
    margin: 0 auto;
    max-width: 1920px;
    background: #ddd;
    box-shadow: 0 0 20px -5px #000;


    font-synthesis: none;
    text-rendering: optimizeLegibility;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    -webkit-text-size-adjust: 100%;
  }

  /* Set default font size and line height */
  html {
    font-size: 13px;
    line-height: 1.5;
    @media screen and (min-width: 450px) {
      font-size: 15px;
    }
    @media screen and (min-width: 768px) {
      font-size: 16px;
    }
  }

  code {
    font-family: source-code-pro, Menlo, Monaco, Consolas, 'Courier New',
    monospace;
  }

  /* Set default link styles */
  a {
    color: inherit;
    text-decoration: none;
  }

  /* Set default button styles */
  button {
    cursor: pointer;
    border: none;
    background-color: transparent;
    font-family: 'Roboto', sans-serif;
  }

  /* Set default heading styles */
  h1,
  h2,
  h3,
  h4,
  h5,
  h6 {
    margin: 0;
    font-weight: 700;
    line-height: 1.2;
    cursor: default;
  }

  h1 {
    font-size: 2rem;
  }

  h2 {
    font-size: 1.75rem;
  }

  h3 {
    font-size: 1.5rem;
  }

  h4 {
    font-size: 1.25rem;
  }

  h5 {
    font-size: 1.15rem;
  }

  h6 {
    font-size: 1rem;
  }

  @media screen and (min-width: 768px) {
    h1 {
      font-size: 3rem;
    }

    h2 {
      font-size: 2.5rem;
    }

    h3 {
      font-size: 2rem;
    }

    h4 {
      font-size: 1.5rem;
    }

    h5 {
      font-size: 1.25rem;
    }

    h6 {
      font-size: 1rem;
    }
  }

  /* Set default paragraph styles */
  p {
    margin: 0;
    font-size: 1.3rem;
    line-height: 1.5;
  }

  /* Set default list styles */
  ul,
  ol {
    margin: 0;
    padding: 0;
  }

  ul {
    list-style: none;
  }

  /* Set default input styles */
  input {
    border: none;
    outline: none;
    background-color: #f5f5f5;
    padding: 0.5rem 1rem;
    font-family: 'Roboto', sans-serif;
    font-size: 1rem;
  }
  
  /* Set default section styles */
  section {
    margin: 0;
  }
`;
export const CardWrapper = styled.section`
  display: block;
  height: 100%;
  overflow: auto;
  padding: 5px;
  border: 4px #333 solid;
  border-radius: 15px;
  box-shadow: 0 0 20px -10px black, inset white 0px 0px 20px 5px;

  @media screen and (min-width: 768px) {
    padding: 10px;
    box-shadow: none;

    &:hover {
      box-shadow: 0 0 20px -10px black, inset white 0px 0px 20px 5px;
    }
  }

  &::-webkit-scrollbar {
    width: 8px;
  }

  &::-webkit-scrollbar-track {
    background-color: transparent;
  }

  &::-webkit-scrollbar-thumb {
    background-color: darkgrey;
    border-radius: 5px;
  }
`;
