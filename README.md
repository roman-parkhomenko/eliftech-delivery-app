# My test project: [Elif delivery app](https://eliftech-delivery-app.netlify.app/)

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app) and deployed with [Netlify](https://app.netlify.com/teams/phenk0/overview).


## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.


## Learn More

You can learn more in the [GitLab repository](https://gitlab.com/roman-parkhomenko/eliftech-delivery-app).